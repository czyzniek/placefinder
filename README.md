# Facebook PlaceFinder

PlaceFinder is Facebook API based project which helps finding places in given city.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Java 8
2. `appId` and `appSecret` for Facebook API. Fill then in file `src/main/resources/application.yml`


### Installing

Run command to start application (witin project directory):

***Windows***

|with default Facebook client  |with client that handles paging in Facebook         |
|------------------------------|----------------------------------------------------|
|mvnw.cmd clean spring-boot:run|mvnw.cmd clean spring-boot:run -Drun.profiles=paging|


***Unix based system***

|with default Facebook client  |with client that handles paging in Facebook         |
|------------------------------|----------------------------------------------------|
|mvnw clean spring-boot:run    |mvnw clean spring-boot:run -Drun.profiles=paging    |

Application should start and be exposed on port 8080. Check `/health` endpoint: http://localhost:8080/health
You should see following response:
```
200 OK
{
    status: "UP",
    diskSpace: {
        status: "UP",
        total: <computer_specific_value>,
        free: <computer_specific_value>,
        threshold: <computer_specific_value>
    }
}
```

## Running the tests

Test are configured to be run during build process. Use Maven goal `test` to start tests:
```
mvnw/mvnw.cmd clean test
```