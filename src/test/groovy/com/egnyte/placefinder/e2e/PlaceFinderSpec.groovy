package com.egnyte.placefinder.e2e

import com.egnyte.placefinder.dto.City
import com.egnyte.placefinder.dto.Place
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PlaceFinderSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    def "should find Egnyte in Poznan"() {
        when:
        List<Place> response = restTemplate.getForObject("/poland/poznan/egnyte", List)

        then:
        response.size() == 1
        response[0].name == "Egnyte Poland"
        response[0].latitude == 52.40474913
        response[0].longitude == 16.940680915
    }

    def "should find bars in both Poznan cities"() {
        when:
        List<City> response = restTemplate.getForObject("/poland/poznan/bar", List)

        then:
        response.size() == 2
        response[0].name == "Posen"
        response[0].country == "Poland"
        response[0].latitude == 52.4
        response[0].longitude == 16.9167
        response[0].places.size() == 25 //deafult profile without paging
        response[1].name == "Poznań, Siedlce, Poland"
        response[1].country == "Poland"
        response[1].latitude == 51.6833
        response[1].longitude == 22.3167
        response[1].places.size() == 25 //deafult profile without paging
    }
}
