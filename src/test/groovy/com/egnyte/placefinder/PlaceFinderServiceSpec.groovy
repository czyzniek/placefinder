package com.egnyte.placefinder

import com.egnyte.placefinder.facebook.SimpleFacebookPlacesClient
import facebook4j.Category
import facebook4j.Place
import facebook4j.ResponseList
import spock.lang.Specification

import java.util.stream.Stream

class PlaceFinderServiceSpec extends Specification {

    def fb = Mock(SimpleFacebookPlacesClient)
    PlaceFinderService service = new PlaceFinderService(fb)

    def "should find one city with one place"() {
        given:
        def cityCategory = Stub(Category) { getName() >> "city" }
        def cityLocation = Stub(Place.Location) {
            getCountry() >> "country"
            getCity() >> "city"
            getLatitude() >> 1.0f
            getLongitude() >> 1.0f
        }
        def foundCity = Stub(Place) {
            getName() >> "city"
            getCategories() >> [cityCategory]
            getLocation() >> cityLocation
        }
        def cityResponse = Stub(ResponseList) { stream() >> Stream.of(foundCity)  }
        def placeLocation = Stub(Place.Location) {
            getLatitude() >> 1.1f
            getLongitude() >> 1.1f
        }
        def foundPlace = Stub(Place) {
            getName() >> "place"
            getLocation() >> placeLocation
        }
        def placesResponse = Stub(ResponseList) { stream() >> Stream.of(foundPlace)  }

        when:
        def result = service.findPlaces("country", "city", "place")

        then:
        2 * fb.fetchPlaces(*_) >> cityResponse >> placesResponse

        then:
        result.size() == 1
        with(result[0]) {
            name == "city"
            country == "country"
            latitude == 1.0f
            longitude == 1.0f
        }
        result[0].places.size() == 1
        with(result[0].places[0]) {
            name == "place"
            latitude == 1.1f
            longitude == 1.1f
        }
    }
}
