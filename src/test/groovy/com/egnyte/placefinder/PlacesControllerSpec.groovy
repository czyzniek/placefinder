package com.egnyte.placefinder

import com.egnyte.placefinder.dto.City
import com.egnyte.placefinder.dto.Place
import com.egnyte.placefinder.facebook.FacebookConnectionException
import io.vavr.control.Try
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.is

@WebMvcTest(PlacesController)
class PlacesControllerSpec extends Specification {

    @Autowired
    MockMvc mvc

    @Autowired
    PlaceFinderService placeFinderService

    def "should return exactly one place"() {
        given:
        def foundCity = Mock(City)
        def egnytePlace = new Place("Egnyte Poland", 52.404167557908f, 16.940044275923f)

        def request = given()
                .mockMvc(mvc)

        when:
        def response = request
                .when()
                .get("/{country}/{city}/{placeName}", "poland", "poznan", "egnyte")

        then:
        1 * foundCity.places >> [egnytePlace]
        1 * placeFinderService.findPlaces("poland", "poznan", "egnyte") >> [foundCity]

        then:
        response.then()
                .statusCode(200)
                .body("[0].name", equalTo("Egnyte Poland"))
                .body("[0].latitude", equalTo(52.404167557908f))
                .body("[0].longitude", equalTo(16.940044275923f))
    }

    def "should return multiple cities with theirs places"() {
        given:
        def foundCity1 = new City("city", "country", 1.0f, 1.0f)
        foundCity1.places = [new Place("place", 1.1f, 1.1f)]
        def foundCity2 = new City("city", "country", 2.0f, 2.0f)
        foundCity2.places = [new Place("place", 2.2f, 2.2f)]

        def request = given()
                .mockMvc(mvc)

        when:
        def response = request
                .when()
                .get("/{country}/{city}/{placeName}", "country", "city", "place")

        then:
        1 * placeFinderService.findPlaces(_ as String, _ as String, _ as String) >> [foundCity1, foundCity2]

        then:
        response.then()
                .statusCode(200)
                .body("size()", is(2))
                .body("[0].name", equalTo("city"))
                .body("[0].country", equalTo("country"))
                .body("[0].latitude", equalTo(1.0f))
                .body("[0].longitude", equalTo(1.0f))
                .body("[0].places.size()", is(1))
                .body("[0].places[0].name", equalTo("place"))
                .body("[0].places[0].latitude", equalTo(1.1f))
                .body("[0].places[0].longitude", equalTo(1.1f))
                .body("[1].name", equalTo("city"))
                .body("[1].country", equalTo("country"))
                .body("[1].latitude", equalTo(2.0f))
                .body("[1].longitude", equalTo(2.0f))
                .body("[1].places.size()", is(1))
                .body("[1].places[0].name", equalTo("place"))
                .body("[1].places[0].latitude", equalTo(2.2f))
                .body("[1].places[0].longitude", equalTo(2.2f))
    }

    def "should return Service Unavailable due to Facebook connectivity error"() {
        given:
        def request = given()
                .mockMvc(mvc)
        when:
        def response = request.when()
                .get("/{country}/{city}/{place}", "country", "city", "place")

        then:
        1 * placeFinderService.findPlaces(_ as String, _ as String, _ as String) >> { throw new FacebookConnectionException("Cannot connect to Facebook!") }

        then:
        response.then()
                .statusCode(503)
                .body("error", equalTo("Could not extract data from Facebook API. Try again later..."))
    }


    @TestConfiguration
    static class MockConfig {
        def detachedMockFactory = new DetachedMockFactory()

        @Bean
        @Primary
        PlaceFinderService placeFinderService() {
            return detachedMockFactory.Mock(PlaceFinderService)
        }
    }
}
