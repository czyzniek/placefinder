package com.egnyte.placefinder.facebook

import facebook4j.Facebook
import facebook4j.FacebookException
import facebook4j.GeoLocation
import facebook4j.Paging
import facebook4j.Place
import facebook4j.Reading
import facebook4j.ResponseList
import spock.lang.Specification

class SimpleFacebookPlacesClientSpec extends Specification {

    def fb = Mock(Facebook)

    def facebookClient = new SimpleFacebookPlacesClient(fb)

    def "should fetch all places"() {
        when:
        facebookClient.fetchPlaces("query", new Reading())

        then:
        1 * fb.searchPlaces("query", _ as Reading) >> Mock(ResponseList)
    }

    def "should throw Exception during fetching places"() {
        when:
        facebookClient.fetchPlaces("query", new Reading())

        then:
        1 * fb.searchPlaces("query", _ as Reading) >> { throw new FacebookException("ERROR!") }

        then:
        FacebookConnectionException ex = thrown()
        ex.message == "ERROR!"
    }

    def "should throw Exception during fetching places around point"() {
        when:
        facebookClient.fetchPlaces("query", new GeoLocation(1.0f, 1.0f), 1, new Reading())

        then:
        1 * fb.searchPlaces("query", _ as GeoLocation, 1, _ as Reading) >> { throw new FacebookException("ERROR!") }

        then:
        FacebookConnectionException ex = thrown()
        ex.message == "ERROR!"
    }
}
