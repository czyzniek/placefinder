package com.egnyte.placefinder.facebook

import facebook4j.Facebook
import facebook4j.GeoLocation
import facebook4j.Paging
import facebook4j.Reading
import facebook4j.ResponseList
import spock.lang.Specification

class PageableFacebookPlacesClientSpec extends Specification {

    def fb = Mock(Facebook)
    def simpleClient = Mock(SimpleFacebookPlacesClient)

    def facebookClient = new PageableFacebookPlacesClient(fb, simpleClient)

    def "should fetch all places with paging"() {
        given:
        def firstPage = Mock(ResponseList) { getPaging() >> Mock(Paging) }
        def nextPage = Mock(ResponseList)

        when:
        facebookClient.fetchPlaces("query", new Reading())

        then:
        1 * simpleClient.fetchPlaces("query", _ as Reading) >> firstPage
        1 * fb.fetchNext(_ as Paging) >> nextPage
    }

    def "should fetch all places within center with paging"() {
        given:
        def firstPage = Mock(ResponseList) { getPaging() >> Mock(Paging) }
        def nextPage = Mock(ResponseList)

        when:
        facebookClient.fetchPlaces("query", new GeoLocation(1.0f, 1.0f), 1, new Reading())

        then:
        1 * simpleClient.fetchPlaces("query", _ as GeoLocation, 1, _ as Reading) >> firstPage
        1 * fb.fetchNext(_ as Paging) >> nextPage
    }
}
