package com.egnyte.placefinder;

import com.egnyte.placefinder.dto.City;
import com.egnyte.placefinder.facebook.FacebookConnectionException;
import io.vavr.collection.HashMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/")
public class PlacesController {

	private final PlaceFinderService service;

	PlacesController(PlaceFinderService service) {
		this.service = service;
	}

	@GetMapping(value = "/{country}/{city}/{placeName}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity getPlaces(@PathVariable String country, @PathVariable String city, @PathVariable String placeName) {
		List<City> cities = service.findPlaces(country, city, placeName);
		if(cities.size() == 1)
			return ResponseEntity.ok(cities.get(0).getPlaces());
		else
			return ResponseEntity.ok(cities);
	}

	@ExceptionHandler(FacebookConnectionException.class)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE)
	public Map<String, String> handleFacebookErrors() {
		return HashMap.of(
				"error", "Could not extract data from Facebook API. Try again later..."
		).toJavaMap();
	}
}
