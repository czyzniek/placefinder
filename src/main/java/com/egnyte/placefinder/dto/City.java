package com.egnyte.placefinder.dto;

import lombok.Data;

import java.util.List;

@Data
public class City {

	private final String name;
	private final String country;
	private final double latitude;
	private final double longitude;
	private List<Place> places;
}
