package com.egnyte.placefinder.dto;

import lombok.Data;

@Data
public class Place {

	private final String name;
	private final Double latitude;
	private final Double longitude;
}
