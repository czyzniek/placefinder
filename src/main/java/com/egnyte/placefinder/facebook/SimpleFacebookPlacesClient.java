package com.egnyte.placefinder.facebook;

import facebook4j.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleFacebookPlacesClient implements FacebookPlacesClient {

	private final Facebook fb;

	public SimpleFacebookPlacesClient(Facebook fb) {
		this.fb = fb;
	}

	@Override
	public ResponseList<Place> fetchPlaces(String query, Reading parameters) {
		return tryOf(() -> fb.searchPlaces(query, parameters),
				ex -> {
					log.error("Could not get data from Facebook for query {}", query);
					log.error(ex.getMessage(), ex);
					return new FacebookConnectionException(ex.getMessage(), ex);
				});
	}

	@Override
	public ResponseList<Place> fetchPlaces(String query, GeoLocation center, int radius, Reading parameters) {
		return tryOf(() -> fb.searchPlaces(query, center, radius, parameters),
				ex -> {
					log.error("Could not get data from Facebook for query {}, center {} and radius {}",
							query, center, radius);
					log.error(ex.getMessage(), ex);
					return new FacebookConnectionException(ex.getMessage(), ex);
				});
	}
}
