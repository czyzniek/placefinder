package com.egnyte.placefinder.facebook;

public class FacebookConnectionException extends RuntimeException {

	public FacebookConnectionException() {
	}

	public FacebookConnectionException(String s) {
		super(s);
	}

	public FacebookConnectionException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public FacebookConnectionException(Throwable throwable) {
		super(throwable);
	}

	public FacebookConnectionException(String s, Throwable throwable, boolean b, boolean b1) {
		super(s, throwable, b, b1);
	}
}
