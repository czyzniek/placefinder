package com.egnyte.placefinder.facebook;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="facebook")
@Data
class FacebookConfiguration {

	private String appId;
	private String appSecret;
}
