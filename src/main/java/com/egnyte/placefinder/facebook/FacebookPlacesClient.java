package com.egnyte.placefinder.facebook;

import facebook4j.GeoLocation;
import facebook4j.Place;
import facebook4j.Reading;
import facebook4j.ResponseList;
import io.vavr.CheckedFunction0;
import io.vavr.control.Try;

import java.util.function.Function;

public interface FacebookPlacesClient {
	ResponseList<Place> fetchPlaces(String query, Reading parameters);

	ResponseList<Place> fetchPlaces(String query, GeoLocation center, int radius, Reading parameters);

	default ResponseList<Place> tryOf(CheckedFunction0<ResponseList<Place>> f, Function<Throwable, ? extends RuntimeException> exceptionHandler) {
		return Try.of(f).getOrElseThrow(exceptionHandler);
	}
}
