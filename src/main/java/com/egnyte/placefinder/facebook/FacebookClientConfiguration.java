package com.egnyte.placefinder.facebook;

import facebook4j.Facebook;
import facebook4j.FacebookFactory;
import facebook4j.auth.AccessToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class FacebookClientConfiguration {

	private final FacebookConfiguration fbConf;

	public FacebookClientConfiguration(FacebookConfiguration fbConf) {
		this.fbConf = fbConf;
	}

	private Facebook facebook4j() {
		final Facebook instance = new FacebookFactory().getInstance();
		instance.setOAuthAppId(fbConf.getAppId(), fbConf.getAppSecret());
		instance.setOAuthAccessToken(new AccessToken(fbConf.getAppId() + "|" + fbConf.getAppSecret()));
		return instance;
	}

	@Configuration
	@Profile("default")
	class DefaultFacebookClientConfiguration {

		@Bean
		public FacebookPlacesClient simpleFacebookPlacesClient() {
			return new SimpleFacebookPlacesClient(facebook4j());
		}
	}

	@Configuration
	@Profile("paging")
	class PagingFacebookClientConfiguration {

		@Bean
		public FacebookPlacesClient pageableFacebookPlacesClient() {
			return new PageableFacebookPlacesClient(facebook4j(), new SimpleFacebookPlacesClient(facebook4j()));
		}
	}
}
