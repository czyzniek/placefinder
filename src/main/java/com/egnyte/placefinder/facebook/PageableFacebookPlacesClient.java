package com.egnyte.placefinder.facebook;

import facebook4j.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PageableFacebookPlacesClient implements FacebookPlacesClient {

	private final Facebook fb;
	private final FacebookPlacesClient simpleClient;

	public PageableFacebookPlacesClient(Facebook fb, FacebookPlacesClient simpleClient) {
		this.fb = fb;
		this.simpleClient = simpleClient;
	}

	@Override
	public ResponseList<Place> fetchPlaces(String query, Reading parameters) {
		return tryOf(() -> fetchAll(simpleClient.fetchPlaces(query, parameters)),
				ex -> {
					log.error("Could not get data from Facebook for query {}", query);
					log.error(ex.getMessage(), ex);
					return new FacebookConnectionException(ex.getMessage(), ex);
				});
	}

	@Override
	public ResponseList<Place> fetchPlaces(String query, GeoLocation center, int radius, Reading parameters) {
		return tryOf(() -> fetchAll(simpleClient.fetchPlaces(query, center, radius, parameters)),
				ex -> {
					log.error("Could not get data from Facebook for query {}, center {} and radius {}",
							query, center, radius);
					log.error(ex.getMessage(), ex);
					return new FacebookConnectionException(ex.getMessage(), ex);
				});
	}

	private ResponseList<Place> fetchAll(final ResponseList<Place> firstResult) throws FacebookException {
		ResponseList<Place> page = firstResult;
		while (true) {
			final Paging<Place> paging = page.getPaging();
			if (paging == null)
				break;
			page = fb.fetchNext(paging);
			firstResult.addAll(page);
		}
		return firstResult;
	}
}
