package com.egnyte.placefinder;

import com.egnyte.placefinder.dto.City;
import com.egnyte.placefinder.dto.Place;
import com.egnyte.placefinder.facebook.FacebookPlacesClient;
import facebook4j.GeoLocation;
import facebook4j.Reading;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
class PlaceFinderService {

	private final FacebookPlacesClient fb;

	PlaceFinderService(FacebookPlacesClient fb) {
		this.fb = fb;
	}

	List<City> findPlaces(String country, String city, String placeName) {
		return fb.fetchPlaces(country + " " + city, new Reading().fields("location", "name", "category_list"))
				.stream()
				.filter(place -> place.getCategories().stream().allMatch(cat -> cat.getName().equalsIgnoreCase("city")))
				.filter(place -> country.equalsIgnoreCase(place.getLocation().getCountry()))
				.filter(place -> city.equalsIgnoreCase(place.getLocation().getCity()))
				.map(place -> new City(place.getName(), place.getLocation().getCountry(),
						place.getLocation().getLatitude(), place.getLocation().getLongitude()))
				.peek(foundCity -> {
					GeoLocation center = new GeoLocation(foundCity.getLatitude(), foundCity.getLongitude());
					final List<Place> places = fb.fetchPlaces(placeName, center, 30000, new Reading().fields("location", "name"))
							.stream()
							.map(foundPlace -> new Place(foundPlace.getName(),
									foundPlace.getLocation().getLatitude(), foundPlace.getLocation().getLongitude()))
							.collect(toList());
					foundCity.setPlaces(places);
				})
				.filter(foundCity -> !foundCity.getPlaces().isEmpty())
				.collect(toList());
	}
}
